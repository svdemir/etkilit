﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace boardLock
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        WebBrowser webBrowser1;

        string enSonUrl = "";

        public void button1_Click(object sender, EventArgs e)
        {
            try
            {
                HtmlElement password = webBrowser1.Document.GetElementById("verification-code");
                password.SetAttribute("value", textBox1.Text.ToUpper());
                var Bclick = webBrowser1.Document.GetElementById("generate-verification-btn");
                Bclick.InvokeMember("click");
            }
            catch (Exception ex)
            {
                MessageBox.Show("İnternet Bağlantı hatası.");
            }
            webBrowser1.DocumentCompleted += WebBrowser1_DocumentCompleted;
        }

        public void WebBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            var webBrowser = sender as WebBrowser;
            webBrowser.DocumentCompleted -= WebBrowser1_DocumentCompleted;
            enSonUrl = webBrowser.Url.ToString();
            if (enSonUrl == "http://www.eba.gov.tr/")               //giriş başarılı ise..
            {
                var procs = Process.GetProcessesByName("osk");
                if (procs.Length != 0)
                {
                    procs[0].Kill();
                }
                Application.Exit();
            }
            else if (enSonUrl == "http://giris.eba.gov.tr/EBA_GIRIS/teksifre.jsp")      //giriş hatası durumunda..
            {
                MessageBox.Show("Şifre Hatalı.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                dynamic document = webBrowser1.Document;
                document.ExecCommand("ClearAuthenticationCache", false, null);
                Hazirlik();
            }
        }


        public void Form1_Load(object sender, EventArgs e)
        {
            PaneliOrtala();
            webBrowser1 = new WebBrowser();
            webBrowser1.Navigate("http://giris.eba.gov.tr/EBA_GIRIS/teksifre.jsp");
            webBrowser1.ScriptErrorsSuppressed = true;
            textBox1.Focus();
        }

        public void Hazirlik()
        {
            webBrowser1 = new WebBrowser();
            webBrowser1.Navigate("http://giris.eba.gov.tr/EBA_GIRIS/teksifre.jsp");
            webBrowser1.ScriptErrorsSuppressed = true;
            textBox1.Focus();
        }

        public void textBox1_Enter(object sender, EventArgs e)
        {
            Process.Start("osk.exe");
        }

        public void btnKapat_Click(object sender, EventArgs e)
        {
            DialogResult cevap = MessageBox.Show("Kapatma işlemini onaylıyor musunuz?", "Kapatma İşlemi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (cevap == DialogResult.Yes)
            {
                Process.Start("ShutDown", "/s -f -t 1");
            }
        }

        public void PaneliOrtala()
        {
            if (this.Controls.Count > 0)
            {
                this.Controls["pictureBox1"].Left = (this.Width - this.Controls["pictureBox1"].Width) / 2;
                this.Controls["pictureBox1"].Top = (this.Height - this.Controls["pictureBox1"].Height) / 2;
                this.Controls["textBox1"].Left = this.Controls["pictureBox1"].Left + 140;
                this.Controls["textBox1"].Top = this.Controls["pictureBox1"].Top + 222;

                this.Controls["button1"].Left = this.Controls["pictureBox1"].Left + 530;
                this.Controls["button1"].Top = this.Controls["pictureBox1"].Top + 223;
            }
        }


    }
}
